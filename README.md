# README #

### What is this repository for? ###

* Transcriptome and network analyses in *Saccharomyces cerevisiae* reveal that amphotericin B and lactoferrin synergy disrupt metal homeostasis and stress response

### How do I get set up? ###

* The source codes are mainly R markdown files and best viewed using Rstudio (https://www.rstudio.com/). The output markdown HTML files can be viewed using any web browser or using this website http://htmlpreview.github.io/

### Contribution guidelines ###

* The source codes are licensed under GNU less public licence. Users can contribute by making comments on the issues tracker or contact me via e-mail (see below). 

### Citation

Pang CN, Lai YW, Campbell LT, Chen SC, Carter DA, Wilkins MR. (2017) Transcriptome and network analyses in Saccharomyces cerevisiae reveal that amphotericin B and lactoferrin synergy disrupt metal homeostasis and stress response. Sci Rep. 12;7:40232. doi: 10.1038/srep40232.

http://www.nature.com/articles/srep40232

Pubmed ID: 28079179 

### Who do I talk to?    ###

* Dr. Ignatius Pang: i (dot) pang (at) unsw (dot) edu (dot) au